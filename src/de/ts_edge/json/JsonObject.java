package de.ts_edge.json;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents an object of a json object
 */
public class JsonObject extends JsonEntity<HashMap<String, JsonEntity<?>>> implements JsonContainer {

    /**
     * Construct an empty object
     */
    public JsonObject() {

        super(new HashMap<>());

    }

    public JsonObject(HashMap<String, JsonEntity<?>> value) {

        super(value);

    }

    /**
     * Construct an object filled with parsed data from the given json string
     *
     * @param json json as string
     */
    public JsonObject(String json) {

        super(new HashMap<>());

        this.parseString(json);

    }

    /**
     * Recursive function which iterates through the json and returns the entity of the given path.
     *
     * For Example:
     * if the path equals "foo.bar.1", the function tries to call the get("bar.1") method of the JsonContainer with "foo" key
     *
     *
     * @see JsonEntity
     * @see JsonContainer
     * @param path path towards an entity (seperated with '.')
     * @throws JsonParseException if string cannot be parsed into a valid path
     * @throws ClassCastException if result is not castable to give class T
     * @return value of the given path
     */
    @Override
    public JsonEntity<?> get(String path) {

        return this.get(path, "\\.");

    }

    @Override
    public JsonEntity<?> get(String path, String separator) {

        String[] subPaths = path.split(separator);

        JsonEntity<?> res = this.getValue().get(subPaths[0]);

        if (subPaths.length > 1 && res instanceof JsonContainer) {

            // remove first element
            List<String> remainingPaths = Arrays.asList(
                    Arrays.copyOfRange(subPaths, 1, subPaths.length)
            );

            res = ((JsonContainer) res).get(
                    String.join(".", remainingPaths)
            );

        } else if (subPaths.length > 1) {

            throw new JsonParseException("Try to access invalid path");

        }

        return res;
    }

    /**
     * returns object as string
     *
     * @return object as string
     */
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder("{");

        for (Map.Entry<String, JsonEntity<?>> e : this.getValue().entrySet()) {

            res.append('"');
            res.append(e.getKey());
            res.append('"');
            res.append(':');
            res.append(e.getValue().toString());
            res.append(',');

        }

        if(this.getValue().size() > 0) {
            res.setLength(res.length() - 1);
        }

        res.append('}');

        return res.toString();
    }

    private void parseString(String json) {

        String j = json.trim();


        if (!j.startsWith("{") || !j.endsWith("}")) {

            throw new JsonParseException("String seems to be invalid json object");

        }

        j = j.substring(1, j.length() - 1).trim();

        String key = null;
        int startValue = -1;

        char openingTag = Character.MIN_VALUE;
        char closingTag = Character.MIN_VALUE;

        int tagCount = 0;

        for (int i = 0; i < j.length(); i++) {

            // get key
            if (key == null) {

                Pattern p = Pattern.compile("\".+?([^(\\\\)]|\\\\)\"\\s*(?=:)");
                Matcher m = p.matcher(j.substring(i));

                if (m.find()) {

                    String found = m.group();

                    i = j.indexOf(found, i) + found.length();

                    key = found.substring(1, found.length() - 1);

                }

                continue;

            }

            // skip whitespace
            if (Character.isWhitespace(j.charAt(i))) {

                continue;

            }

            if (startValue == -1) {

                // array
                if (j.charAt(i) == '[') {

                    openingTag = '[';
                    closingTag = ']';

                }
                // object
                else if (j.charAt(i) == '{') {

                    openingTag = '{';
                    closingTag = '}';

                }

                //string
                else if (j.charAt(i) == '"') {

                    Pattern p = Pattern.compile("\"([^\"\\\\]|\\\\\")*?\"");
                    Matcher m = p.matcher(j.substring(i));

                    if (m.find()) {

                        String found = m.group();

                        this.getValue().put(key, JsonData.of(found));

                        i = j.indexOf(found, i) + found.length();

                        key = null;
                        continue;


                    } else {

                        throw new JsonParseException("Cannot parse value: " + j.substring(i));

                    }

                }

                startValue = i;


            }

            if (j.charAt(i) == openingTag) {
                tagCount++;
            }

            if (j.charAt(i) == closingTag) {
                tagCount--;
            }

            if (
                    tagCount == 0 &&
                            (j.charAt(i) == ',' || i == j.length() - 1)
            ) {

                // end of string, increment i
                if (i == j.length() - 1) {

                    i++;

                }



                String valueString = j.substring(startValue, i).trim();

                if (openingTag != Character.MIN_VALUE) {

                    if (openingTag == '[') {

                        this.getValue().put(key, new JsonArray(valueString));

                    } else {

                        this.getValue().put(key, new JsonObject(valueString));

                    }

                } else {

                    this.getValue().put(key, JsonData.of(valueString));

                }

                openingTag = Character.MIN_VALUE;
                closingTag = Character.MIN_VALUE;

                key = null;
                startValue = -1;


            }

        }






        // something went wrong, last object was not parsed
        if (openingTag != Character.MIN_VALUE || startValue != -1) {

            throw new JsonParseException("Not able to parse json string");

        }


    }
}
