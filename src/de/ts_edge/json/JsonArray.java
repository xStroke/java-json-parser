package de.ts_edge.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * <p>Represents an array of a json string.</p>
 * <p>Call getValue() function to access its content via ArrayList&lt;JsonEntity&lt;?&gt;&gt;</p>
 *
 * @author xStroke
 */
public class JsonArray extends JsonEntity<ArrayList<JsonEntity<?>>> implements JsonContainer {

    /**
     * Construct an empty json array
     */
    public JsonArray() {
        super();
    }


    /**
     * Construct a json array with given value
     */
    public JsonArray(ArrayList<JsonEntity<?>> value) {
        super(value);
    }

    /**
     * Construct a json array filled with the parsed data from the given string
     *
     * @param jsonArray String in json format which will be parsed into a json array
     * @throws JsonParseException throws an exception if the given string is obviously not a json array
     */
    public JsonArray(String jsonArray) {

        super(new ArrayList<>());

        this.parseString(jsonArray);

    }

    /**
     * Parse json array to String
     *
     * @return Object as String
     */
    @Override
    public String toString() {
        return this.getValue().toString();
    }

    /**
     * <p>
     * Recursive function which iterates through the json and returns the entity of the given path.
     * </p>
     * <p>
     * For Example:
     * </p>
     * <p>
     * if the path equals "0.bar.0" and the json equals [{"bar": [1, 2, 3]}] the function will return 1
     * </p>
     *
     * @param path path towards an entity (seperated with '.')
     * @return value of the given path
     * @throws JsonParseException if string cannot be parsed into a valid path\
     */
    @Override
    public JsonEntity<?> get(String path) throws JsonParseException, ClassCastException {

        return this.get(path, "\\.");

    }

    /**
     * <p>
     * Recursive function which iterates through the json and returns the entity of the given path separated by given separator
     * </p>
     * <p>
     * For Example:
     * </p>
     * <p>
     * if the path equals "0.bar.0", the separator equals "\\." and the json equals [{"bar": [1, 2, 3]}] the function will return 1
     * </p>
     *
     * @param path      path towards an entity (seperated with '.')
     * @param separator regex which will be used to split the path string
     * @return value of the given path
     * @throws JsonParseException if string cannot be parsed into a valid path\
     * @see String The path String will be separated by the String.split() function
     */
    @Override
    public JsonEntity<?> get(String path, String separator) {
        String[] subPaths = path.split(separator);

        try {

            int index = Integer.parseInt(subPaths[0]);

            JsonEntity<?> res = this.getValue().get(index);

            if (subPaths.length > 1 && res instanceof JsonContainer) {

                // remove first element
                List<String> remainingPaths = Arrays.asList(
                        Arrays.copyOfRange(subPaths, 1, subPaths.length)
                );

                res = ((JsonContainer) res).get(
                        String.join(".", remainingPaths)
                );

            } else if (subPaths.length > 1) {

                throw new JsonParseException("Try to access invalid path: " + path);

            }

            return res;

        } catch (NumberFormatException e) {

            throw new JsonParseException("Try to access JsonArray via String key: " + subPaths[0]);

        }
    }

    private void parseString(String jsonArray) throws JsonParseException {

        String j = jsonArray.trim();

        if (
                !(j.startsWith("[") && j.endsWith("]"))
        ) {
            throw new JsonParseException("Object is not a json array");
        }

        j = j.substring(1, j.length() - 1).trim();


        int start = 0;
        boolean tracking = false;

        char openingTag = Character.MIN_VALUE;
        char closingTag = Character.MIN_VALUE;

        int tagCount = 0;


        for (int i = 0; i < j.length(); i++) {

            if (Character.isWhitespace(j.charAt(i))) {

                continue;

            }


            if (!tracking) {

                // array
                if (j.charAt(i) == '[') {

                    openingTag = '[';
                    closingTag = ']';

                }

                // object
                else if (j.charAt(i) == '{') {

                    openingTag = '{';
                    closingTag = '}';

                }

                //string
                else if (j.charAt(i) == '"') {

                    Pattern p = Pattern.compile("\"([^\"\\\\]|\\\\\")*?\"");
                    Matcher m = p.matcher(j.substring(i));

                    if (m.find()) {

                        String found = m.group();

                        this.getValue().add(JsonData.of(found.substring(found.indexOf('"'))));

                        i = j.indexOf(found, i) + found.length();

                        start = i + 1;

                        continue;


                    } else {

                        throw new JsonParseException("Cannot parse value: " + j.substring(i));

                    }

                }

                tracking = true;

            }

            if (j.charAt(i) == openingTag) {
                tagCount++;
            }

            if (j.charAt(i) == closingTag) {
                tagCount--;
            }


            if (
                    tagCount == 0 &&
                            (j.charAt(i) == ',' || i == j.length() - 1)
            ) {

                // end of string, increment i
                if (i == j.length() - 1) {

                    i++;

                }

                String valueString = j.substring(start, i).trim();

                if (openingTag != Character.MIN_VALUE) {

                    if (openingTag == '[') {

                        this.getValue().add(new JsonArray(valueString));

                    } else {

                        this.getValue().add(new JsonObject(valueString));

                    }

                } else {

                    this.getValue().add(JsonData.of(valueString));

                }

                openingTag = Character.MIN_VALUE;
                closingTag = Character.MIN_VALUE;

                start = i + 1;


                tracking = false;

            }


        }

        // something went wrong, last object was not parsed
        if (openingTag != Character.MIN_VALUE || tracking) {

            throw new JsonParseException("Not able to parse json string");

        }

    }

}
