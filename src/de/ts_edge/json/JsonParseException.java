package de.ts_edge.json;

import java.io.PrintStream;
import java.io.PrintWriter;

public class JsonParseException extends RuntimeException {

    private String message;

    public JsonParseException(String message) {
        super();

        this.message = message;

    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void printStackTrace() {

        System.err.println(JsonParseException.class.getName() + ": " + this.message);


        for(StackTraceElement e : getStackTrace()) {
            System.err.println("\tat " + e);
        }

    }

    @Override
    public void printStackTrace(PrintStream s) {

        s.println(JsonParseException.class.getName() + ": " + this.message);


        for(StackTraceElement e : getStackTrace()) {
            s.println("\tat " + e);
        }

    }

    @Override
    public void printStackTrace(PrintWriter s) {

        s.println(JsonParseException.class.getName() + ": " + this.message);


        for(StackTraceElement e : getStackTrace()) {
            s.println("\tat " + e);
        }

    }
}
