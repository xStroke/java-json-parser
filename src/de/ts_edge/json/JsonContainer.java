package de.ts_edge.json;

/**
 * Interface for json entities with children (Object, Array)
 *
 * @author xStroke
 */
public interface JsonContainer {

    /**
     * <p>
     *     Recursive function which iterates through the json and returns the entity of the given path.
     * </p>
     * <p>
     *     For Example:
     * </p>
     * <p>
     *     If the path equals "foo.bar.0" and the json equals {"foo": {"bar": [1, 2, 3]}} the function will return 1.
     * </p>
     *
     *
     * @param path path towards an entity
     * @throws JsonParseException if string cannot be parsed into a valid path
     * @return value of the given path
     */
    JsonEntity<?> get(String path);


    /**
     * <p>
     *     Recursive function which iterates through the json and returns the entity of the given path separated by given separator
     * </p>
     * <p>
     *     For Example:
     * </p>
     * <p>
     *     If the path equals "foo.bar.0", the separator equals "\\." and the json equals {"foo": {"bar": [1, 2, 3]}} the function will return 1.
     * </p>
     *
     *
     * @see String The path will be separated by the String.split() function
     *
     * @param path path towards an entity (seperated with '.')
     * @param separator regex which will be used to split the path string
     * @throws JsonParseException if string cannot be parsed into a valid path
     * @return value of the given path
     */
    JsonEntity<?> get(String path, String separator);
}
