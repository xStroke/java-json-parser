package de.ts_edge.json;

import java.math.BigInteger;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonData<T> extends JsonEntity<T> {

    public JsonData() {
        super();
    }

    @Override
    public String toString() {
        if (this.value != null) {

            if(this.value instanceof String) {

                return '"' + this.value.toString() + '"';

            }

            return this.value.toString();
        }


        return "null";
    }

    public JsonData(T value) {
        super(value);
    }

    public static JsonData<?> of(String data) {
        return JsonData.parseString(data);
    }

    private static JsonData<?> parseString(String data) {

        if (data.matches("-?\\d+")) {

            if (data.length() > 10 ) {

                return new JsonData<>(new BigInteger(data));

            } else if (data.length() == 10 && data.compareTo("" + Integer.MAX_VALUE) > 0) {

                return new JsonData<>(new BigInteger(data));

            } else {

                return new JsonData<>(Integer.parseInt(data));

            }




        } else if (data.matches("-?\\d+\\.\\d+")) {

            return new JsonData<>(Double.parseDouble(data));

        } else if (data.equals("null")) {

            return new JsonData<>();

        } else if (data.matches("(true)|(false)")) {

            return new JsonData<>(Boolean.parseBoolean(data));

        } else if (data.matches("\"([^\"]|\\\\\")*?\"")) {

            Pattern p = Pattern.compile("\\\\[\"\\\\]");
            Matcher m = p.matcher(data);

            String dataString = m.replaceAll(
                    MatchResult::group
            );

            return new JsonData<>(dataString.substring(1, dataString.length() - 1));

        } else {

            throw new JsonParseException("Cannot parse value: " + data);

        }

    }

}
