package de.ts_edge.json;

/**
 * Abstract class that represents every entity of a json object
 *
 * @param <T> Type of entities value
 * @author xStroke
 */
public abstract class JsonEntity<T> {

    /**
     * Value of the entity
     */
    protected T value;

    /**
     * Construct an empty JsonEntity
     */
    public JsonEntity() {

        this.value = null;

    }

    /**
     * Construct JsonEntity with given value
     *
     * @param value value of the entity
     */
    public JsonEntity(T value) {

        this.value = value;

    }

    @Override
    public abstract String toString();

    public T getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(T value) {
        this.value = value;
    }

    public static JsonEntity<?> of (String json) throws JsonParseException {

        String trimmedJson = json.trim();

        if (trimmedJson.startsWith("{") && trimmedJson.endsWith("}")) {
            return new JsonObject(trimmedJson);
        }

        if (trimmedJson.startsWith("[") && trimmedJson.endsWith("]")) {
            return new JsonArray(trimmedJson);
        }

        return JsonData.of(json);

    }
}
