import de.ts_edge.json.JsonArray;
import de.ts_edge.json.JsonData;
import de.ts_edge.json.JsonParseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class JsonArrayTest extends JsonTest {

    @Test
    void array() {

        JsonTest.printHeadline("test arrays");

        Assertions.assertEquals(
                "[]",
                new JsonArray("[]").toString()
        );

        System.out.println("(1) succeeded");

        Assertions.assertEquals(
                "[1, 2.45, \"Hello,World\", true, false, null]",
                new JsonArray("[1,2.45,\"Hello,World\",true,false,null]").toString()
        );

        System.out.println("(2) succeeded");

        Assertions.assertEquals(
                "[1, 2.45, \"Hello World\", true, false, null, \"\"]",
                new JsonArray(
                        "[\n" +
                                "\t1, \n" +
                                "\t2.45 , \n" +
                                "\t \"Hello World\", \n" +
                                "true  , \t    false,    null,\n" +
                                "\n \t \"\"   ]"
                ).toString()
        );

        System.out.println("(3) succeeded");

        Assertions.assertEquals(
                1.0,
                new JsonArray("[1.0,2.0]").get("0").getValue()
        );

        System.out.println("(4) succeeded");

        Assertions.assertEquals(
                3,
                new JsonArray("[0, [1, 2, [3, 4]]]").get("1.2.0").getValue()
        );

        System.out.println("(5) succeeded");

    }

    @Test
    void exceptionArray() {

        JsonTest.printHeadline("test exception arrays");

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> new JsonArray("not an array")
        );

        System.out.println("(1) succeeded");

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> new JsonArray("[]]")
        );

        System.out.println("(2) succeeded");

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> new JsonArray("[[]")
        );

        System.out.println("(3) succeeded");

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> new JsonArray("[],[]")
        );

        System.out.println("(4) succeeded");

        Assertions.assertThrowsExactly(
                IndexOutOfBoundsException.class,
                () -> new JsonArray("[1,2,3]").get("42")
        );

        System.out.println("(5) succeeded");

    }

}
