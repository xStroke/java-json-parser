import de.ts_edge.json.JsonData;
import de.ts_edge.json.JsonEntity;
import de.ts_edge.json.JsonParseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class JsonDataTest {

    @Test
    void dataTypes() {

        JsonTest.printHeadline("Test All Data Types");

        Assertions.assertEquals(
                new JsonData<>(true).getValue(),
                JsonData.of("true").getValue()
        );

        System.out.println("(1) succeeded");

        Assertions.assertEquals(
                new JsonData<>(false).getValue(),
                JsonData.of("false").getValue()
        );

        System.out.println("(2) succeeded");

        Assertions.assertEquals(
                new JsonData<>(null).getValue(),
                JsonData.of("null").getValue()
        );

        System.out.println("(3) succeeded");

        Assertions.assertEquals(
                new JsonData<>("hello \" \\ world").getValue(),
                JsonData.of("\"hello \\\" \\\\ world\"").getValue()
        );

        System.out.println("(4) succeeded");

        Assertions.assertEquals(
                new JsonData<>("").getValue(),
                JsonData.of("\"\"").getValue()
        );

        System.out.println("(5) succeeded");

        Assertions.assertEquals(
                new JsonData<>(1).getValue(),
                JsonData.of("1").getValue()
        );

        System.out.println("(6) succeeded");

        Assertions.assertEquals(
                new JsonData<>(-1).getValue(),
                JsonData.of("-1").getValue()
        );

        System.out.println("(7) succeeded");

        Assertions.assertEquals(
                new JsonData<>(1.5).getValue(),
                JsonData.of("1.5").getValue()
        );

        System.out.println("(8) succeeded");

        Assertions.assertEquals(
                new JsonData<>(-1.5).getValue(),
                JsonData.of("-1.5").getValue()
        );

        System.out.println("(9) succeeded");

    }

    @Test
    void exceptionDataTypes() {

        JsonTest.printHeadline("Exception Data Types");

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> JsonData.of("")
        );

        System.out.println("(1) succeeded");

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> JsonData.of("not a data type")
        );

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> JsonData.of("\"broken \" String\"")
        );

        System.out.println("(2) succeeded");

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> JsonData.of("1.3.3")
        );

        System.out.println("(3) succeeded");

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> JsonData.of("1a2b3c")
        );

        System.out.println("(4) succeeded");

        Assertions.assertThrowsExactly(
                JsonParseException.class,
                () -> JsonEntity.of("1,5")
        );

        System.out.println("(5) succeeded");

    }

}
