import de.ts_edge.json.JsonArray;
import de.ts_edge.json.JsonContainer;
import de.ts_edge.json.JsonEntity;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JsonFileTest {

    @Test
    void testFile() throws IOException {

        JsonTest.printHeadline("Test Json File");

        String path = "src/test/resources/example.json";

        Path p = Path.of(path);

        JsonArray e = new JsonArray(Files.readString(p));

        assertEquals(
                "61a680ce917f0948326131a0",
                e.get("0._id").getValue()
        );

        System.out.println("(1) succeeded");

        assertEquals(
                "61a680ce917f0948326131a0",
                e.get("0#_id", "#").getValue()
        );

        System.out.println("(1) succeeded");

    }

}
