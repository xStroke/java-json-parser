import java.util.Locale;

public class JsonTest {

    public static void printHeadline(String title) {

        System.out.println("---------------------");
        System.out.println(title.toUpperCase(Locale.ROOT));
        System.out.println("---------------------");

    }

}
