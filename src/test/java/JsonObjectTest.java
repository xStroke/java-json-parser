import de.ts_edge.json.JsonData;
import de.ts_edge.json.JsonEntity;
import de.ts_edge.json.JsonObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

public class JsonObjectTest {

    @Test
    void object() {

        JsonTest.printHeadline("test objects");

        Assertions.assertEquals(
                "{}",
                new JsonObject("{}").toString()
        );

        System.out.println("(1) succeeded");

        HashMap<String, JsonEntity<?>> map = new HashMap<>();

        map.put("hello", new JsonData<>("world"));
        map.put("int", new JsonData<>(1));
        map.put("double", new JsonData<>(1.545));
        map.put("null", new JsonData<>(null));
        map.put("object", new JsonObject("{\"foo\": \"bar\\\"test\"}"));

        Assertions.assertEquals(
                map.toString(),
                new JsonObject("{\"hello\": \"world\", \"int\": 1, \"double\": 1.545, \"null\": null, \"object\": {\"foo\": \"bar\\\"test\"}}").getValue().toString()
        );

        System.out.println("(2) succeeded");

    }

}
