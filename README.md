# Java Json Parser

This is my attempt on a simple java json parser. I used plain Java and Regex to parse Json strings into Java classes.

The library was programmed with Gradle 7.1
and Java 11. 

## Contents
- [Installation](#installation)
- [Usage](#usage)

## Installation
- [Gradle](#gradle)
- [Maven](#maven)
- [Available Packages](#available-packages)

### Gradle
Expand your `build.gradle` with the following lines of code.

Add `implementation 'de.ts_edge:java-json-parser:_VERSION_'` to the `dependencies` object and replace `_VERSION_` with the package version of your choice.

``` groovy
dependencies {
    [...]

    implementation 'de.ts_edge:java-json-parser:_VERSION_'
}

```
Add this git repo to your repositories object like so. Otherwise, Gradle cannot find this library.
``` groovy
repositories {
    maven {
        name "java-json-parser"
        url "https://gitlab.com/api/v4/projects/31672673/packages/maven"
    }
}
```


### Maven

Expand your `pom.xml` with the following lines of code.

Add a `dependency` tag to the `dependencies` and replace its `_VERSION_` with the package version of your choice.

```xml
<dependencies>
    [...]
    <dependency>
        <groupId>de.ts_edge</groupId>
        <artifactId>java-json-parser</artifactId>
        <version>_VERSION_</version>
    </dependency>
</dependencies>
```

Add this git repo to your `repositories` tag like so. Otherwise, Maven cannot find this library.
```xml
<repositories>
    [...]
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/31672673/packages/maven</url>
    </repository>
</repositories>
```

### Available Packages

Latest: 1.0.3

<details>
  <summary>All</summary>

#### Containing serious bugs (fixed in later versions)
  - 1.0.2
  - 1.0.1
  - 1.0

</details>

## Usage

Let's say the following json is stored in a variable called `json`
```json
{
  "foo": "bar",
  "array": [
    {"hello": 1, "world":  2},
    {"hello": 3.3, "world":  4.4}
  ]
}
```
First we import the `JsonEntiy` class and parse the `json` string via the `JsonEntity.of()` method. Then we are able to access its data by using the `get()` method.
```java
import de.ts_edge.json.JsonEntity;

public class Example {
    
    public void exampleFunction(String json) {
        
        JsonEntity<?> entity = JsonEntity.of(json);
        
        System.out.println(
                entity.get("array.1.world").getValue()
        );
        
    }
    
}
```
If we call `exampleFunction()`, `4.4` will be printed on the console.

See [JavaDoc](https://gitlab.com/xStroke/java-json-parser/-/tree/main/doc) for more information.